[![pipeline status](https://gitlab.com/jeremymtan/jeremy-tan-ids-721-individual-1/badges/main/pipeline.svg)](https://gitlab.com/jeremymtan/jeremy-tan-ids-721-individual-1/-/commits/main)

# Static Site with Zola

[portfolio webiste](https://jeremy-tan.vercel.app)

## Screenshot of Website

![Screenshot_2024-03-10_at_4.34.57_AM](/uploads/31e10b8672b99cfa93b3ec93abcb7782/Screenshot_2024-03-10_at_4.34.57_AM.png)

## Youtube Video

https://youtu.be/FELH2sbDKEw

## Purpose

The purpose of this project is to create a static personal website with Zola. I build upon my previous mini-project 1 but add more information about myself and include the projects I worked on this semester for the course.

## Preparation

1. Install Zola
2. Create website with `zola init`
3. Add a theme for css `cd themes` then `git submodule add <theme name>`
4. Create pages in the `content` folder
5. `zola serve` to test website or `zola build`
6. Install Vercel CLI
7. Login to Vercel with `vercel login`
8. Link your repo with `vercel link`
9. Copy your Vercel token, Vercel team id, and Vercel Org id to Gitlab secrets
10. Push repo to run pipeline filr `.gitlab-ci.yml`

## References

1. https://vercel.com/guides/how-can-i-use-gitlab-pipelines-with-vercel#configuring-gitlab-ci/cd-for-vercel
2. https://www.getzola.org/themes/hyde/
3. https://www.getzola.org/documentation/deployment/vercel/
