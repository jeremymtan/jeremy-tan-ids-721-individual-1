+++
title = "Databricks ETL Pipleline"
date = 2023-11-10
weight = 3
+++
The Data Extraction and Transformation Pipeline project aims to retrieve and process tennis match data from fivethirtyeight datasets, specifically using Databricks in conjunction with the Databricks API and various Python libraries.
[Project Link](https://github.com/nogibjj/Jeremy_Tan_IDS706_Week11_Individual)


